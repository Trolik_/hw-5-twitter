"use strict";

const GetUsers = 'https://ajax.test-danit.com/api/json/users';
const GetPosts = 'https://ajax.test-danit.com/api/json/posts';

class Card {
	constructor(postId, title, body, userId, name, email) {
		this.postId = postId;
		this.title = title;
		this.body = body;
		this.userId = userId;
		this.name = name;
		this.email = email;
	}

	render() {
		const card = document.createElement('div');

		card.id = `${this.postId}`;
		card.classList.add('card');
		card.innerHTML = `
            <div class="flexTitleAndBtn">
                <div class="cardBtn">
                    <button class="btn" onclick="deleteCard(${this.postId})">X</button>
                </div>
                <h2 class="cardTitle">${this.title}</h2>
            </div>
            <span class="cardText">${this.body}</span>
            <span class="cardUserName">Author: ${this.name}</span>
            <span class="cardUserMail">E-mail: ${this.email}</span>            
        `;
		return card;
	}
}

fetch(`${GetUsers}`)
	.then(response => response.json())
	.then(users => {
		fetch(`${GetPosts}`)
			.then(response => response.json())
			.then(posts => {
				const cardsContainer = document.querySelector('.cardsContainer');

				posts.forEach(post => {
					const user = users.find(user => user.id === post.userId)
					const card = new Card(
						post.id,
						post.title,
						post.body,
						user.id,
						user.name,
						user.email
					)
					cardsContainer.append(card.render())
				})
			})
			.catch(error => postMessage.innerHTML = "Error: " + error + ".")
	})
	.catch(error => postMessage.innerHTML = "Error: " + error + ".")

function deleteCard(postId) {
	fetch(`${GetPosts}/${postId}`, {
		method: 'DELETE',
	})
		.then(response => {
			if (response.ok) {
				document.getElementById(`${postId}`).remove()
			} else {
				alert(`An error occurred in card with postId: ${postId}. Cannot delete!!!`)
			}
		})
		.catch(error => postMessage.innerHTML = "Error: " + error + ".")
}



